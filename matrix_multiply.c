#include <stdio.h>

#ifndef _HARD_VALUE
# define MATRIX_SIZE 10
#endif

void	init_matrix(int A[MATRIX_SIZE][MATRIX_SIZE], int B[MATRIX_SIZE][MATRIX_SIZE])
{
  int val;

  val = 1;
  for (int i = 0; i < MATRIX_SIZE; i++)
    for (int j = 0; j < MATRIX_SIZE; j++)
    {
      A[i][j] = val;
      B[i][j] = val;
      val++;
    }
}

void  init_res(int res[MATRIX_SIZE][MATRIX_SIZE])
{
  for (int i = 0; i < MATRIX_SIZE; i++)
    for (int j = 0; j < MATRIX_SIZE; j++)
      res[i][j] = 0;
}

void	matrix_mul(int A[MATRIX_SIZE][MATRIX_SIZE], int B[MATRIX_SIZE][MATRIX_SIZE], 
    int res[MATRIX_SIZE][MATRIX_SIZE])
{
  for (int i = 0; i < MATRIX_SIZE; i++)
    for (int j = 0; j < MATRIX_SIZE; j++)
      for (int k = 0; k < MATRIX_SIZE; k++)
        res[i][j] += A[i][k] * B[k][j];
}

int	main(void)
{
  int A[MATRIX_SIZE][MATRIX_SIZE];
  int B[MATRIX_SIZE][MATRIX_SIZE];
  int res[MATRIX_SIZE][MATRIX_SIZE];

  init_matrix(A, B);
  init_res(res);
  matrix_mul(A, B, res);

#ifdef _DEBUG
  for (int i = 0; i < MATRIX_SIZE; i++)
  {
    for (int j = 0; j < MATRIX_SIZE; j++)
    {
      printf("[%02d]", res[i][j]);
    }
    printf("\n");
  }
  printf("\n");
#endif
}
