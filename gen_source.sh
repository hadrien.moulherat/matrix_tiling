#!/bin/sh

for MATRIX_SIZE in $(seq 1 100)
do
  DEST="./all_code/code_$MATRIX_SIZE.asm"
  
  echo -e ".eqv\tEXIT2 93\n.eqv\tMATRIX_SIZE $MATRIX_SIZE" > "$DEST"

  echo -e >> "$DEST"
  cat algo_asm >> "$DEST"
  echo -e >> "$DEST"

  ((MATRIX_VAR=$MATRIX_SIZE*$MATRIX_SIZE))
  ((MATRIX_LEN=$MATRIX_VAR*4))

  echo -e ".data\n.align 2\n\nA:" >> "$DEST"

  for i in $(seq 1 $MATRIX_VAR)
  do
    printf ".word 0x%x\n" "$i" >> "$DEST"
  done

  echo -e "\nB:" >> "$DEST"

  for i in $(seq 1 $MATRIX_VAR)
  do
    printf ".word 0x%x\n" "$i" >> "$DEST"
  done

  echo -e "\nres:\t.space $MATRIX_LEN" >> "$DEST"
done
