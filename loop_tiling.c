#include <stdio.h>

#ifndef _HARD_VALUE
# define MATRIX_SIZE 10
#endif

void	init_matrix(int A[MATRIX_SIZE][MATRIX_SIZE], int B[MATRIX_SIZE][MATRIX_SIZE])
{
  int val;

  val = 1;
  for (int i = 0; i < MATRIX_SIZE; i++)
    for (int j = 0; j < MATRIX_SIZE; j++)
    {
      A[i][j] = val;
      B[i][j] = val;
      val++;
    }
}

void  init_res(int res[MATRIX_SIZE][MATRIX_SIZE])
{
  for (int i = 0; i < MATRIX_SIZE; i++)
    for (int j = 0; j < MATRIX_SIZE; j++)
      res[i][j] = 0;
}

int matrix_mul(int A[MATRIX_SIZE][MATRIX_SIZE], int B[MATRIX_SIZE][MATRIX_SIZE], int C[MATRIX_SIZE][MATRIX_SIZE])
{
  int acc00, acc01, acc10, acc11;
  int i, j, k, ii, kk, ib = 5, kb = 5;

  for (ii = 0; ii < MATRIX_SIZE; ii += ib)
  {
    for (kk = 0; kk < MATRIX_SIZE; kk += kb)
    {
      for (j = 0; j < MATRIX_SIZE; j += 2)
      {
        for (i = ii; i < ii + ib; i += 2)
        {
          if (kk == 0)
            acc00 = acc01 = acc10 = acc11 = 0;
          else
          {
            acc00 = C[i + 0][j + 0];
            acc01 = C[i + 0][j + 1];
            acc10 = C[i + 1][j + 0];
            acc11 = C[i + 1][j + 1];
          }
          for (k = kk; k < kk + kb; k++)
          {
            acc00 += B[k][j + 0] * A[i + 0][k];
            acc01 += B[k][j + 1] * A[i + 0][k];
            acc10 += B[k][j + 0] * A[i + 1][k];
            acc11 += B[k][j + 1] * A[i + 1][k];
          }
          C[i + 0][j + 0] = acc00;
          C[i + 0][j + 1] = acc01;
          C[i + 1][j + 0] = acc10;
          C[i + 1][j + 1] = acc11;
        }
      }
    }
  }
}

int	main(void)
{
  int A[MATRIX_SIZE][MATRIX_SIZE];
  int B[MATRIX_SIZE][MATRIX_SIZE];
  int res[MATRIX_SIZE][MATRIX_SIZE];

  init_matrix(A, B);
  init_res(res);
  matrix_mul(A, B, res);

#ifdef _DEBUG
  for (int i = 0; i < MATRIX_SIZE; i++)
  {
    for (int j = 0; j < MATRIX_SIZE; j++)
    {
      printf("[%02d]", res[i][j]);
    }
    printf("\n");
  }
  printf("\n");
#endif
}
