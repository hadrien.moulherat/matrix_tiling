.eqv    EXIT2 93
.eqv	MATRIX_SIZE 10
.eqv	TILING_SIZE 5

.text
_start:
	call main
	
	li	a0, 0
	li	a7, EXIT2
	ecall
	ebreak

main:
	addi	sp, sp, -40
	sw	ra, 0(sp)
	sw	s0, 4(sp)	#MATRIX_SIZE
	sw	s1, 8(sp)	#TILING_SIZE
	sw	s2, 12(sp)	#A
	sw	s3, 16(sp)	#B
	sw	s4, 20(sp)	#res
	sw	s5, 24(sp)	#acc00
	sw	s6, 28(sp)	#acc01
	sw	s7, 32(sp)	#acc10
	sw	s8, 36(sp)	#acc11
	
	li	s0, MATRIX_SIZE
	li	s1, TILING_SIZE
	la	s2, A
	la	s3, B
	la	s4, res
	
	li	a0, 0		#ii = 0
for_ii:
	bge	a0, s0, end_for_ii
	
	li	a1, 0		#kk = 0
for_kk:
	bge	a1, s0, end_for_kk

	li	a2, 0		#j = 0
for_j:
	bge	a2, s0, end_for_j

	mv	a3, a0		#i = ii
	li	t0, 0
	add	t0, a0, s1	#t0 = ii + ib
for_i:
	bge	a3, t0, end_for_i
	
	#  -- if / else section --
	
	beq	a1, zero, else
	
	mul	t2, a3, s0 	#i * MAXTRIX_SIZE
	add	t3, t2, s0	#(i+1) * MAXTRIX_SIZE
	
	#get res[i][j] value on t4
	add	t4, t2, a2
	slli	t4, t4, 2
	add	t4, t4, s4	#@ of res[i][j]
	
	lw	t5, 0(t4)	#t5 = res[i][j]
	lw	t4, 4(t4)	#t4 = res[i][j+1]
	
	mv	s5, t5
	mv	s6, t4
	
	#get res[i+1][j] value on t4
	add	t4, t3, a2
	slli	t4, t4, 2
	add	t4, t4, s4	#@ of res[i+1][j]
	
	lw	t5, 0(t4)	#t5 = res[i+][j]
	lw	t4, 4(t4)	#t4 = res[i+1][j+1]
	
	mv	s7, t5
	mv	s8, t4

	j	init_fork_k

else:
	li	s5, 0
	li	s6, 0
	li	s7, 0
	li	s8, 0

	#  -- end if / else section --

init_fork_k:
	mv	a4, a1		#k = kk
	li	t1, 0
	add	t1, a1, s1
for_k:
	bge	a4, t1, end_for_k
	
	#  -- compute section --
	
	mul	t2, a4, s0 	#k * MAXTRIX_SIZE
	mul	t3, a3, s0 	#i * MAXTRIX_SIZE
	add	t4, t3, s0	#(i+1) * MAXTRIX_SIZE
	
	#get B[k][j] value on t5
	add	t5, t2, a2
	slli	t5, t5, 2
	add	t5, t5, s3	#@ of B[k][j]
	
	#get A[i][k] value on t6
	add	t6, t3, a4
	slli	t6, t6, 2
	add	t6, t6, s2	#@ of A[i][k]
	
	#get A[i+1][k] value on t2
	add	t2, t4, a4
	slli	t2, t2, 2
	add	t2, t2, s2	#@ of A[i+1][k]
	
	lw	t3, 0(t5)	#t3 = B[k][j]
	lw	t4, 0(t6)	#t4 = A[i][k]
	mul	t3, t3, t4	#t3 = B[k][j] * A[i][k]
	add	s5, s5, t3	#acc00 += B[k][j] * A[i][k]
	
	lw	t3, 4(t5)
	lw	t4, 0(t6)
	mul	t3, t3, t4
	add	s6, s6, t3	#acc01 += B[k][j+1] * A[i][k]
	
	lw	t3, 0(t5)
	lw	t4, 0(t2)
	mul	t3, t3, t4
	add	s7, s7, t3	#acc10 += B[k][j] * A[i+1][k]
	
	lw	t3, 4(t5)
	lw	t4, 0(t2)
	mul	t3, t3, t4
	add	s8, s8, t3	#acc11 += B[k][j+1] * A[i+1][k]
	
	#  -- end compute section --
	
	addi	a4, a4, 1
	j	for_k
	
end_for_k:
	#  -- compute section 2 --
	
	mul	t2, a3, s0 	#i * MAXTRIX_SIZE
	add	t3, t2, s0	#(i+1) * MAXTRIX_SIZE
	
	#get res[i][j] value on t4
	add	t4, t2, a2
	slli	t4, t4, 2
	add	t4, t4, s4	#@ of res[i][j]
	
	sw	s5, 0(t4)	#t5 = res[i][j]
	sw	s6, 4(t4)	#t4 = res[i][j+1]
	
	#get res[i+1][j] value on t4
	add	t4, t3, a2
	slli	t4, t4, 2
	add	t4, t4, s4	#@ of res[i+1][j]
	
	sw	s7, 0(t4)	#t5 = res[i+][j]
	sw	s8, 4(t4)	#t4 = res[i+1][j+1]
	
	#  -- end compute section 2 --
	
	addi	a3, a3, 2
	j	for_i

end_for_i:
	addi	a2, a2, 2
	j	for_j
	
end_for_j:
	add	a1, a1, s1
	j	for_kk

end_for_kk:	
	add	a0, a0, s1
	j	for_ii

end_for_ii:
	lw	s8, 36(sp)
	lw	s7, 32(sp)
	lw	s6, 28(sp)
	lw	s5, 24(sp)
	lw	s4, 20(sp)
	lw	s3, 16(sp)
	lw	s2, 12(sp)
	lw	s1, 8(sp)
	lw	s0, 4(sp)
	lw	ra, 0(sp)
	addi	sp, sp, 40
	ret

.data
.align 2

A:		
.word 0x1
.word 0x2
.word 0x3
.word 0x4
.word 0x5
.word 0x6
.word 0x7
.word 0x8
.word 0x9
.word 0xa
.word 0xb
.word 0xc
.word 0xd
.word 0xe
.word 0xf
.word 0x10
.word 0x11
.word 0x12
.word 0x13
.word 0x14
.word 0x15
.word 0x16
.word 0x17
.word 0x18
.word 0x19
.word 0x1a
.word 0x1b
.word 0x1c
.word 0x1d
.word 0x1e
.word 0x1f
.word 0x20
.word 0x21
.word 0x22
.word 0x23
.word 0x24
.word 0x25
.word 0x26
.word 0x27
.word 0x28
.word 0x29
.word 0x2a
.word 0x2b
.word 0x2c
.word 0x2d
.word 0x2e
.word 0x2f
.word 0x30
.word 0x31
.word 0x32
.word 0x33
.word 0x34
.word 0x35
.word 0x36
.word 0x37
.word 0x38
.word 0x39
.word 0x3a
.word 0x3b
.word 0x3c
.word 0x3d
.word 0x3e
.word 0x3f
.word 0x40
.word 0x41
.word 0x42
.word 0x43
.word 0x44
.word 0x45
.word 0x46
.word 0x47
.word 0x48
.word 0x49
.word 0x4a
.word 0x4b
.word 0x4c
.word 0x4d
.word 0x4e
.word 0x4f
.word 0x50
.word 0x51
.word 0x52
.word 0x53
.word 0x54
.word 0x55
.word 0x56
.word 0x57
.word 0x58
.word 0x59
.word 0x5a
.word 0x5b
.word 0x5c
.word 0x5d
.word 0x5e
.word 0x5f
.word 0x60
.word 0x61
.word 0x62
.word 0x63
.word 0x64

B:
.word 0x1
.word 0x2
.word 0x3
.word 0x4
.word 0x5
.word 0x6
.word 0x7
.word 0x8
.word 0x9
.word 0xa
.word 0xb
.word 0xc
.word 0xd
.word 0xe
.word 0xf
.word 0x10
.word 0x11
.word 0x12
.word 0x13
.word 0x14
.word 0x15
.word 0x16
.word 0x17
.word 0x18
.word 0x19
.word 0x1a
.word 0x1b
.word 0x1c
.word 0x1d
.word 0x1e
.word 0x1f
.word 0x20
.word 0x21
.word 0x22
.word 0x23
.word 0x24
.word 0x25
.word 0x26
.word 0x27
.word 0x28
.word 0x29
.word 0x2a
.word 0x2b
.word 0x2c
.word 0x2d
.word 0x2e
.word 0x2f
.word 0x30
.word 0x31
.word 0x32
.word 0x33
.word 0x34
.word 0x35
.word 0x36
.word 0x37
.word 0x38
.word 0x39
.word 0x3a
.word 0x3b
.word 0x3c
.word 0x3d
.word 0x3e
.word 0x3f
.word 0x40
.word 0x41
.word 0x42
.word 0x43
.word 0x44
.word 0x45
.word 0x46
.word 0x47
.word 0x48
.word 0x49
.word 0x4a
.word 0x4b
.word 0x4c
.word 0x4d
.word 0x4e
.word 0x4f
.word 0x50
.word 0x51
.word 0x52
.word 0x53
.word 0x54
.word 0x55
.word 0x56
.word 0x57
.word 0x58
.word 0x59
.word 0x5a
.word 0x5b
.word 0x5c
.word 0x5d
.word 0x5e
.word 0x5f
.word 0x60
.word 0x61
.word 0x62
.word 0x63
.word 0x64

res:	.space 400
